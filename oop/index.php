<?php
    // TODO: Release 0
    require_once('animal.php');
    $sheep = new Animal("Shaun");

    echo "Name : " . $sheep->name . "<br>";
    echo "Legs : " . $sheep->legs . "<br>";
    echo "Cold Blooded : " . $sheep->cold_blooded . "<br>";
    echo "<br><br>";

    // TODO: Release 1
    require_once('ape.php');
    $sungokong = new Ape("Kera Sakti");
    echo "Name : " . $sungokong->name . "<br>";
    echo "Legs : " . $sungokong->legs . "<br>";
    echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>";
    echo "Yell : ";
    $sungokong-> yell() . "<br>";
    echo "<br><br>";

    require_once('frog.php');
    $kodok = new Frog("Buduk");
    echo "Name : " . $kodok->name . "<br>";
    echo "Legs : " . $kodok->legs . "<br>";
    echo "Cold Blooded : " . $kodok->cold_blooded . "<br>";
    echo "Jump : ";
    $kodok-> jump() . "<br>";
    echo "<br><br>";

?>