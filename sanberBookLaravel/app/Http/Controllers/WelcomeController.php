<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function welcome(Request $request){
        // dd($request->all());
        $nama = $request['FirstName'] . " " . $request['LastName'];
        return view('welcome', ['name' => $nama]);
    }
}
