<!DOCTYPE html>
<html>
    <head>
        <title>Form Page</title>
    </head>
    <body>
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
        <form action="/welcome" method='POST'>
            @csrf
            <label>First Name:</label><br>
            <input type="text" name="FirstName"><br><br>
            <label>Last Name:</label><br>
            <input type="text" name="LastName"><br><br>
            <label>Gender:</label><br>
            <input type="radio" name="gender">Male<br>
            <input type="radio" name="gender">Female<br>
            <input type="radio" name="gender">Other<br><br>
            <label>Nationality:</label><br>
            <select name="Nationality">
                <option value="Indonesian">Indonesian</option>
                <option value="American">American</option>
            </select><br><br>
            <label>Language Spoken:</label><br>
            <input type="checkbox" name="language" value="Bahasa">Bahasa Indonesia<br>
            <input type="checkbox" name="language" value="English">English<br>
            <input type="checkbox" name="language" value="other">Other<br><br>
            <label>Bio:</label><br>
            <textarea name="bio" rows="10" cols="30"></textarea><br><br>
            <input type="submit" value="Sign Up">

        </form>
    </body>
</html>